module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      fontFamily: {
        body: ['Inter', 'sans-serif'],
        heading: ['Acta Display', 'serif'],
        actaBook: ['Acta Book', 'serif'],
        actaHeadline: ['Acta Headline', 'serif'],
        actaDisplay: ['Acta Display', 'serif'],
        actaAgate: ['Acta Agate', 'serif'],
      },
      colors: {
        red: '#B10925',
        gray: {
          100: '#f5f5f5',
          200: '#eeeeee',
          300: '#e0e0e0',
          400: '#bdbdbd',
          500: '#9e9e9e',
          600: '#757575',
          700: '#616161',
          800: '#424242',
          900: '#212121',
        },
        cleargray: '#EDEDED',
      },
    },
  },
  plugins: [],
}
