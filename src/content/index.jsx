const data = {
  title: `Habitación 106:
  El Juicio a Nicolás Zepeda
  `,
  description: `Hace cinco años, la estudiante japonesa <strong>Narumi Kurosaki</strong> desapareció en Besanzón, Francia.

  Todos los indicios seguidos por las autoridades locales apuntaban a la responsabilidad de su expareja, <strong>Nicolás Zepeda Contreras</strong>. La investigación reveló un tormentoso romance, la posesiva personalidad del joven chileno y sus sospechosos movimientos por Europa.
  
  Este martes 12 de abril, un tribunal francés declaró a Zepeda en primera instancia como culpable del asesinato de Narumi, y fijó una condena de 28 años de cárcel por estimar que actuó con premeditación. 
  
  La Tercera reconstruyó la historia a partir de las declaraciones originales de sus protagonistas, las que se recrearon en una dramatización que busca explicar más claramente lo que se sabe y lo que no de este misterioso episodio.
  
  Así presentamos, en 10 capítulos, las claves de un caso criminal que conmocionó a tres países.
  `,
  audiointro: `https://interactivo.latercera.com/zepeda-assets/v2/INTRO%20GRAL%20CON%20MUSICA.mp3`,
  chapters: [
    {
      audio: `https://interactivo.latercera.com/zepeda-assets/v2/CAP%201%20FX.mp3`,
      audiointro: `https://interactivo.latercera.com/zepeda-assets/INTRO%20CAP%201.mp3`,
      length: '4:52',
      prefix: `Episodio 1`,
      title: 'El acusado',
      description: `Durante el juicio fue retratado como “mentiroso”, “manipulador” y “narcisista”. La familia de la víctima incluso lo calificó de “monstruo”. Sus padres, en cambio, describieron a un joven ejemplar. ¿Quién es realmente Nicolás Zepeda?`,
      coordinates: {
        x: 1915,
        y: 1787,
      },
      script: `
**Narrador:** 

La solitaria rutina que Nicolás Zepeda llevaba en la Butte y fue acompañado por los gendarmes a la van que lo trasladaría hasta el Palacio de Justicia de Besanzón. El vehículo fue escoltado por policías motorizados y llegó a las 8, dos horas antes del inicio de la audiencia.

En el tribunal se había montado una compleja operación para recibirlo. Hasta poco antes de la apertura de los alegatos, un equipo logístico se aseguraba de que los 100 mil euros invertidos por el estado francés facilitaran el desarrollo de un juicio sin precedentes; debían coordinar testigos en tres continentes, crear un sistema de traducción simultánea al español y al japonés, y habilitar dos salas extras para casi 60 periodistas. Todo con el fin de determinar si Zepeda mató a su exnovia, Narumi Kurosaki, quien lleva cinco años desaparecida.

Cuando llegó la hora, Zepeda entró a la sala con una camisa celeste y corbata azul. Tenía en frente a sus padres, Humberto Zepeda y Ana Luz Contreras, y a la madre y hermana de Narumi. Delante de todos ellos, declaró su inocencia:

**Nicolás Zepeda:**  Hace cinco años que tengo presente a Narumi en mis pensamientos y la inmensa pena de su familia, de su mamá. No hay día que pase en que no los tenga presentes. Quiero decir que yo no maté a nadie: niego con toda mi fuerza los cargos.

**Narrador:** 

La investigación tenía múltiples indicios contra Zepeda. Era la última persona que había estado con Narumi y su conducta en los días previos y posteriores a la desaparición resultaba difícil de explicar. Para la familia de la joven japonesa no caben dudas. Taeko Akiyama, su madre, así lo manifestó durante el juicio.

**Taeko Akiyama:** Nunca dirá la verdad, ni ahora ni después. No hay que dejar nunca más a este monstruo en libertad. ¡Quiero matar a este hombre! Voy a seguir protegiendo a todas las mujeres y el precio de mi voluntad será mi vida.

**Narrador:** 

La convicción de la mamá de Narumi era compartida por una buena parte de los testigos del caso. La tarea de la defensa lucía cuesta arriba. ¿Cómo entender que un joven chileno, aparentemente ejemplar, hubiera asesinado a su expareja? Para intentarlo, hay que retroceder tres décadas…

**Narrador:** 

Nicolás Zepeda nació en Temuco en 1990 y es el mayor de tres hermanos. Su familia tenía un buen pasar; Humberto Zepeda era un alto ejecutivo regional de Telefónica y Ana Luz Contreras trabajaba como secretaria en la administración municipal de René Saffirio. Desde su infancia, destacó como estudiante en el Colegio Centenario. Su promedio general al salir de la educación media en 2008 fue 6.6; en matemáticas, su mejor ramo, terminó con un 7.0. Estas notas le permitieron entrar a ingeniería comercial en la Universidad de Chile. Su padre destacó estas cualidades en el primer día del juicio.

**Humberto Zepeda:** Ha sido siempre muy responsable y emprendedor. Una persona que no le haría daño a nadie.

**Narrador:** 

Pero Zepeda no solo estaba interesado en los números; era un joven de intereses diversos. Era fanático del_animé_y se inspiraba en esa estética para dibujar personajes femeninos. También le apasionaba la tecnología y escribía reseñas de celulares en sitios como FayerWayer. Zepeda vivía en internet. Usaba una plataforma para publicar fotos y mantenía un blog en el cual escribía reflexiones y cuentos. En uno de ellos,titulado "La llamada", relata la historia de Daniel, un hombre que viaja desde Chile a Estados Unidos para reencontrarse con una ex polola. Ella ya ha superado la tormentosa relación y se encuentra emparejada con otra persona. En un cuento posterior, Daniel reprocha a su antigua polola por rechazarlo.

**Nicolás Zepeda:** Y no me atrevo a decirte que nunca más escribiré sobre ti. Quizás siempre escriba un poquito de ti, sobre ti, pensando en ti. Es lo que estoy haciendo ahora. Es una manera de decirte que, aunque no me llames y no me hables más, siempre te voy a querer.

**Narrador:** 

Zepeda se fue a estudiar a Santiago en 2009, mientras que su familia dejó Temuco para iniciar un periplo por Antofagasta, La Serena y Viña del Mar. Su interés por la cultura japonesa lo llevó a postular a una beca para irse de intercambio a ese país. Fue ahí donde conoció a Narumi y comenzó la historia que hoy lo tiene como presunto asesino.
 
      `,
    },
    {
      audio: `https://interactivo.latercera.com/zepeda-assets/v2/Cap%202%20FX.mp3`,
      audiointro: `https://interactivo.latercera.com/zepeda-assets/INTRO%20CAP%202.mp3`,
      length: '5:25',
      prefix: `Episodio 2`,
      title: `Relaciones tormentosas`,
      description: `En 2014, una beca del gobierno japonés le permitió a Zepeda entrar en la Universidad de Tsukuba como estudiante de intercambio. Allí conoció a Narumi Kurosaki, con quien inició un intenso y problemático romance.
      `,
      coordinates: {
        x: 3005,
        y: 1200,
      },
      script: `
**Narrador:**

Antes de que empezaran sus problemas judiciales y desapareciera de internet, la plataforma Last FM indicaba que Nicolás Zepeda tenía como artistas favoritos a David Guetta y Calvin Harris. Quizás ese gusto por la eléctronica lo llevó a probarse como DJ en Japón. En octubre de 2014, mientras ponía música en una fiesta, conoció a Narumi Kurosaki. La joven japonesa también estudiaba en la Universidad de Tsukuba, aunque era casi cinco años menor. Por entonces, Zepeda salía con otra estudiante, pero terminó con ella para comprometerse con Narumi. Ambos comenzaron una relación más seria en febrero de 2015.

Taeko Akiyama conoció a Zepeda en marzo de ese año, cuando este ayudó a Narumi con una mudanza. Ella recuerda que el chileno mostró interés por la historia familiar. Su situación económica era más compleja. Los padres de Narumi eran separados y ella, como hermana mayor, trabajaba a medio tiempo para ayudar a su mamá. Zepeda también pidió ver los álbumes de fotos, parecía un joven cariñoso. Akiyama se llevó una buena primera impresión, pero al poco tiempo se dio cuenta de que también era un hombre celoso.

**Taeko Akiyama:** Sé que Nicolás le ordenaba a Narumi que eliminara de las redes sociales a sus amigos hombres y le prohibía que estuviera en su departamento con hombres, siendo que Nicolás se quedaba con una amiga en su departamento.

**Narrador:**

Nicolás Zepeda regresó a Chile en mayo de 2015. Akiyama recuerda que el chileno lloró amargamente en el aeropuerto. La pareja intentó mantenerse a distancia, pero terminaban y volvían constantemente. Zepeda se ofreció a pagarle a Narumi un pasaje a Chile para tratar de revivir la relación. Ella estuvo en Chile entre septiembre y octubre de 2015. Conoció a la familia de su novio y recorrió Valparaíso y Santiago. Luego tuvieron que volver a separarse, pero Zepeda sugirió que buscaría una forma de volver a Japón apenas terminara su carrera.

Y así lo hizo. En abril de 2016, ya estaba de regreso en Tokio. Buscó algún trabajo o una pasantía que le permitiera establecerse, pero pasaron los meses y no tuvo éxito. Además, Narumi ya tenía otros planes. Había postulado a una beca para estudiar administración y finanzas en la Universidad del Franco-Condado, en Besanzón. En agosto, cuando Narumi partió a Francia, los problemas de la pareja se agravaron. Sus intercambios por teléfono o mensaje de texto se volvieron cada día más violentos, particularmente del lado de Zepeda. Este le exigía eliminar de redes sociales a sus nuevos amigos de la universidad.

El 5 de septiembre, por ejemplo, intercambiaron 646 tensos mensajes.

**Nicolás Zepeda:** Destruiste todo con tus mentiras. Me haces ver como tonto porque no estoy en Francia.

**Narumi Kurosaki:** No voy a aceptar que me llames mentirosa.

**Nicolás Zepeda:** Demuéstrame que eres seria, quiero ver tu compromiso. Decides defender a tres tipos que conociste la semana pasada. ¿Quieres que les envíe un mensaje por Facebook diciendo que eres mi pareja y que dejen de andar detrás de ti?

**Narumi Kurosaki:** Nunca suprimiré a Rafa y Arthur. Hablamos más tarde.

**Nicolás Zepeda:** Voy a perder la paciencia, Narumi. Me has tratado como basura.

**Narrador:** 

Dos días después de esa pelea, Zepeda subió un amenazante video a Dailymotion. Mirando directamente a la cámara y hablando en inglés, le recordó unas condiciones que le había exigido para salvar la relación. Cuando le consultaron sobre esta grabación en el juicio explicó que se trataba de un testimonio personal, como escribir en un diario íntimo.

**Nicolás Zepeda:** Narumi hizo algunas cosas malas que le costaron tener que seguir ciertas condiciones para mantener esta relación (…) Si sigue las condiciones por dos semanas, las dejaré sin efecto (…) Ella debe reconstruir la confianza y pagar un poco del costo de lo que hizo y asumir si va a cometer esos errores con una persona que la quiere.

**Narrador:** 

El quiebre definitivo ocurrió el 8 de octubre. Narumi le reprochó a Zepeda su falta de apoyo con un supuesto embarazo, que nunca fue corroborado. Los investigadores franceses tradujeron la conversación del inglés al español de la siguiente forma:

**Narumi Kurosaki:** Jamás olvidaré que me embarazaste; nunca te hiciste responsable de mi embarazo, de mi hijo.

**Nicolás Zepeda:** Recuerda, yo quería que verificáramos eso antes de que te fueras (…) Vine a Japón y tú te escapaste a Francia. Nos destruiste con tu egoísmo.

**Narumi Kurosaki:** Vete a la mierda.

**Narrador:** 

Esta fue la última conversación que ambos tuvieron hasta la inesperada visita de Zepeda a Francia. Un mes después, el chileno regresó al país e inmediatamente se internó en una clínica especialista en desórdenes conductuales. Cuando se le preguntó sobre esta situación durante el juicio, aseguró que no tenía nada que ver con su quiebre con Narumi.

**Nicolás Zepeda:**  Sufrí un choque cultural inverso.
      `,
    },
    {
      audio: `https://interactivo.latercera.com/zepeda-assets/v2/CAP%203%20FX.mp3`,
      audiointro: `https://interactivo.latercera.com/zepeda-assets/INTRO%20CAP%203.mp3`,
      length: '4:26',
      prefix: `Episodio 3`,
      title: `Material inflamable`,
      description: `Dos meses después del quiebre, Zepeda viajó a Europa. Dice que quería analizar la posibilidad de continuar sus estudios allá. Después de un inusual recorrido por los bosques de la zona y de realizar extrañas compras en un supermercado, terminó frente a la puerta de Narumi.`,
      coordinates: {
        x: 890,
        y: 782,
      },
      script: `
**Narrador:** 

Luego de hacer escala en Madrid y aterrizar en Ginebra, Nicolás Zepeda llegó en bus a la ciudad francesa de Dijon el miércoles 30 de noviembre de 2016. A las 5:17 de la tarde, recogió en la misma estación el Renault Scenic que había reservado por 237 euros un par de semanas antes. También cargó una tarjeta telefónica de prepago y recorrió la ciudad. Ya de noche, manejó una hora hasta Besanzón, donde se encuentra la Universidad del Franco-Condado. Precisamente ahí estudiaba Narumi Kurosaki.

Zepeda llegó a los alrededores del campus después de medianoche y decidió pasar la noche en el auto. La pregunta era inevitable en el juicio.

**Fiscal Manteaux:** ¿Por qué dormir en el auto como un vagabundo?

**Nicolás Zepeda:** Cuestión de dinero.

**Fiscal Manteaux:** ¿Cuánto es el sueldo mínimo en Chile?

**Nicolás Zepeda:** No lo sé.

**Fiscal Manteaux:** Eso da cuenta de que usted nunca tuvo apuros económicos. Son 400 euros. Usted gastó 1.350 euros. Cuando uno gasta ese dinero, no se priva de gastar 35 euros en un alojamiento.

**Narrador:** 

Zepeda regresó a Dijon cerca del mediodía del jueves 1 de diciembre. Un poco antes de las cuatro, pasó al supermercado Carrefour del mall Toison d’Or y se llevó un bidón de cinco litros de combustible para estufa marca Winflamm, un detergente con cloro y una caja de fósforos. La cuenta fue de 9,80 euros. Esta extraña compra es uno de los indicios más relevantes para la fiscalía de la presunta responsabilidad de Zepeda en la desaparición de Narumi. ¿Para qué podía necesitar esos materiales inflamables?

**Nicolás Zepeda:**  Trabajé en una agencia de arriendo de autos en Estados Unidos y me enseñaron que siempre hay que tener un bidón para cargar combustible si las estaciones están lejos (…)** **El bidón lo compré porque lo vi en oferta y se ajustaba a la necesidad que tenía en ese momento. No sabía si lo iba a necesitar.

**Narrador:** Durante el juicio, Zepeda también explicó que necesitaba el detergente para limpiar una mancha de chocolate derretido en el auto y que la caja de fósforos tenía un diseño que simplemente le pareció bonito. Cuando le tocó su turno, el abogado querellante Randall Schwerdorffer no pudo evitar la ironía.

**Abogado Schwerdorffer**: ¡Así que el único _souvenir_ que se llevó de Dijon fue una caja de fósforos! ¡Y lo compró junto a un combustible!

**Nicolás Zepeda:** Si no estuviera acusado, a nadie le extrañaría.

**Narrador:** 

Las compras no fueron las únicas actividades de esa tarde que Zepeda tuvo que explicar. Según los registros del GPS de su auto, Zepeda recorrió caminos secundarios del departamento vecino de Jura, una zona de exuberante vegetación y corrientes de agua. Finalmente se detuvo por casi tres horas en un punto oriental del bosque de Chaux.


**Nicolás Zepeda:**  Es un viaje de Dijon a Besanzón en el que quise conocer los pequeños pueblos que hay en la ruta. Hay luces de navidad, árboles adornados; me llama la atención cómo viven eso en invierno.

**Narrador:** 

Esa noche, el auto de Zepeda nuevamente fue detectado en el campus de la Universidad del Franco-Condado. Su sospechosa presencia se repetiría en días posteriores, hasta el reencuentro con Narumi. Finalmente, el 2 de diciembre, Zepeda tomó una habitación en el hotel-restorán “La Table de Gustave” en Ornans, un pueblo emplazado 25 kilómetros al sur de Besançon. Fue en este lugar donde pasó más tiempo, como afirman testigos.

De sus actividades diurnas no se sabe demasiado, pero el registro de la tarjeta de crédito ubica a Zepeda en Besanzón. En la tarde del 3 de diciembre entró en la tienda H&M de la ciudad y se llevó una chaqueta azul y una camisa blanca por 80 euros. El juez Matthieu Husson le preguntó al acusado si había comprado la ropa teniendo en mente la cita con Narumi; este aseguró que no.

Al día siguiente, Zepeda estrenó su nueva tenida cuando se reencontró con su expareja.
      `,
    },
    {
      audio: `https://interactivo.latercera.com/zepeda-assets/v2/CAP%204%20FX.mp3`,
      audiointro: `https://interactivo.latercera.com/zepeda-assets/INTRO%20CAP%204.mp3`,
      length: '4:51',
      prefix: `Episodio 4`,
      title: `Sombra en el campus`,
      description: `En los días previos a la desaparición de Narumi, las cámaras de seguridad captaron a una persona merodeando su residencia. Varios estudiantes dicen haber visto a un hombre sospechoso en el edificio. ¿Dónde estaba Zepeda en esos momentos?`,
      coordinates: {
        x: 2856,
        y: 2793,
      },
      script: `
**Narrador:** 

Al segundo día del juicio, la abogada de Nicolás Zepeda, Jacqueline Laffont, protestó con fuerza el intento de la fiscalía y los querellantes por introducir una prueba hasta entonces desconocida para la defensa y para los jueces. Se trataba de un video de las cámaras de seguridad de la Universidad del Franco-Condado que data del 1 de diciembre de 2016. Las imágenes mostraban a una persona de abrigo negro y capucha rondando la residencia Theodor Rousseau, donde Narumi tenía su habitación.

En realidad no se trataba de una nueva prueba, sino de un descubrimiento reciente del investigador David Borne al revisar un video que siempre estuvo dentro del expediente.

**David Borne**: Tiene un comportamiento sospechoso. Mientras seguía mi investigación, me di cuenta de que a esa hora, a las 12.32 am, Nicolás Zepeda estaba en el campus. A las 1.11am vemos al mismo hombre pasar frente a la cámara. Luego, lo vemos tomando una foto en el frontis del edificio. Esta sombra pasa varias veces durante la noche por una zona habitualmente desierta.

**Narrador:** 

Zepeda había llegado esa tarde a Besanzón y efectivamente estaba allí, estacionado en el campus, a la hora en que el registro fue tomado. Él mismo lo reconoció. Lógicamente, su negativa fue categórica cuando le preguntaron si era él quien aparecía en las imágenes.

Había otras pruebas relacionadas con el misterioso personaje, potencialmente más difíciles de contrarrestar que un video de mala resolución. Habían sido incluidas en los primeros meses de la investigación. Se trataba de los testimonios de dos estudiantes extranjeras, Rachel Roberts y Nadia Ouaked, quienes se habían encontrado dentro de la misma residencia con un hombre ajeno a la universidad. Roberts declaró que la tarde del 2 de diciembre se topó de golpe con un sujeto de abrigo negro y guantes en la cocina del primer piso.


**Rachel Roberts:** Cuando abrí la puerta dio un pequeño salto hacia atrás y desapareció. Me habló algo con acento americano. Parecía estar ocultándose, actuaba de forma sospechosa (…) Cuando después me mostraron las fotos comencé a llorar, porque entendí que podía ser el asesino.

**Narrador:** 

La policía le presentó a Roberts un set de ocho fotos para que identificara al hombre de negro; ella señaló la foto de Nicolás Zepeda. Lo mismo hizo la argelina Nadia Ouaked cuando fue consultada. Ella recordaba a un individuo de 24-25 años, ojos verdes, de apariencia hispana y pelo negro. Al igual que Roberts, se lo había encontrado en la cocina esa misma semana.

**Nadia Ouaked:** La puerta estaba cerrada, aunque siempre está abierta. Las luces estaban apagadas. Tomé una olla para calentar leche. Las ventanas estaban cerradas, hacía mucho frío. Cuando me di vuelta, me sorprendió ver a un chico sentado en el suelo, con la cabeza en su regazo. Me habló en un idioma extraño; creo que era español. Tenía los ojos rojos, como si hubiera llorado durante horas. Me explicó que se había desmayado. Se lavó la cara y se fue.

**Narrador:** 

Cuando el abogado querellante Schwerdorffer tuvo la oportunidad de interrogar a Ouaked, primero le pidió a Zepeda que se pusiera de pie y se quitara la máscara. Este obedeció. Luego se dirigió a la testigo.

**Abogado Schwerdorffer**: ¿Le recuerda algo este rostro? Mire con atención…

**Nadia Ouaked:** Sí, es la persona con la que hablé. No es una cara que vaya a olvidar, porque recién había llegado a la residencia.

**Narrador:** 

La defensa de Zepeda cuestionó la validez del sistema de identificación fotográfico, pero no le hizo preguntas a Ouaked. Para los abogados acusadores, los testimonios eran complementos perfectos a las cámaras de seguridad. El fiscal Manteaux destacó un detalle importante: la descripción entregada por Roberts calzaba con la vestimenta que el hombre registrado en video estaba usando ese mismo día, el 2 de diciembre. Luego agregó que el auto de Zepeda siempre se encontraba en el campus cuando el misterioso sujeto de negro se metía en el encuadre de la cámara. Esto ocurrió 11 veces en los días previos a la desaparición de Narumi.

El chileno, una vez más, negó todo lo que había sido expuesto.

**Nicolás Zepeda:**  La primera vez que entré a ese edificio fue con Narumi. Encuentro que estos testimonios son muy extraños. Yo no soy esa persona, solo es una hipótesis. Como todos, estoy tratando de encontrar una explicación lógica.
      `,
    },
    {
      audio: `https://interactivo.latercera.com/zepeda-assets/v2/CAP%205%20FX.mp3`,
      audiointro: `https://interactivo.latercera.com/zepeda-assets/INTRO%20CAP%205.mp3`,
      length: '5:26',
      prefix: `Episodio 5`,
      title: `Habitación 106`,
      description: `La expareja se reunió en la tarde del 4 de diciembre. Zepeda asegura que fue algo “fortuito”, una versión que sus acusadores consideran absolutamente inverosímil. La cita terminaría a altas horas de la noche, entre gritos, en la pieza de Narumi Kurosaki.`,
      coordinates: {
        x: 1691,
        y: 2780,
      },
      script: `
**Narrador:** 

Nicolás Zepeda recuerda que el encuentro del 4 de diciembre fue casual. Es cierto que su presencia constante en el campus de la Universidad del Franco-Condado creaba una oportunidad, pero él prefiere verlo como algo natural. Su versión es que estaba en el auto cuando Narumi le golpeó la ventana del lado del pasajero. Ella venía llegando de sus clases de baile y de haber pasado la noche junto a su nueva pareja, Arthur del Piccolo, uno de los amigos de Narumi que Zepeda quería eliminar de sus redes sociales.

A los investigadores del caso les cuesta creer que los eventos de esa jornada hayan comenzado así. Durante el juicio, el fiscal Manteaux le preguntó -exasperado- cómo era posible que hubiera viajado desde tan lejos y pasado tanto tiempo en el campus sin acercarse a ella.

**Nicolás Zepeda:**  No estaba seguro de poder materializar la idea que tenía en mente. Estaba avergonzado de mí mismo… Tenía la esperanza de verla. Pensaba que sería bueno hablar de nuestra ruptura.

**Narrador:** 

Como no existen más versiones con los detalles del reencuentro, Zepeda tiene cierto control sobre la historia de lo que ocurrió esa tarde. Según cuenta, Narumi estaba sorprendida, pero feliz de verlo. Se pusieron al día y él la invitó a cenar al hotel donde se había registrado unos días antes, La Table de Gustave, en el pueblito de Ornans. Ella aceptó. En una selfie tomada a las 6.07 frente a una iglesia de la zona, ambos lucen contentos. Llegaron al restorán cerca de las 7. Los meseros recuerdan que ambos estaban bien vestidos y que el ambiente era cordial, pero que no parecía una cena romántica. Zepeda pagó la cena con su tarjeta de crédito.

Al salir del restorán, el chileno se habría ofrecido a llevarla de vuelta a Besanzón. El viaje tomaba 25 minutos. Narumi le habría dicho que el camino de regreso podía ser peligroso y le sugirió que podía quedarse con ella en la residencia. Pasadas las 10, enfilaron de vuelta a Besanzón en el Renault, que superó los 80 kilómetros por hora en una zona de 70 justo cinco kilómetros antes de entrar en la ciudad. La cámara de seguridad ubicada frente a la residencia de Narumi registró la llegada de ambos a las 10.58. Zepeda parecía entrar con su maleta.

Entre las 3.15 y 3.21 de la mañana del 5 de diciembre, 15 vecinos de Narumi escucharon gritos de dolor y ruidos sordos en una de las habitaciones. Algunos empezaron a compartir sus impresiones a través de un chat. Rachel Roberts, la misma que reconoció a Zepeda dentro de la residencia un par de días antes, escribió:

**Rachel Roberts:**  Se diría que están matando a alguien.

**Narrador:** 

Solo uno de los residentes, Adrien Laurent, salió al pasillo del primer piso para averiguar qué pasaba, pero no logró ubicar de dónde venía la bulla y regresó a su pieza apenas volvió el silencio. A nadie más se le ocurrió llamar a la policía.

Zepeda aseguró en el juicio que nunca escuchó ningún grito; que él y Narumi tuvieron relaciones sexuales y se durmieron. Pero en su primera declaración, la de diciembre de 2016, el chileno había insinuado que los gritos que se oyeron en la residencia eran de Narumi, pero de placer. No era la única contradicción en sus testimonios. También había dicho que dejó la residencia a la mañana siguiente, el 5 de diciembre, cuando el GPS de su auto no registró movimiento hasta la madrugada del 6. Había un “día perdido”.

Según su última versión, Zepeda pasó las siguientes 24 horas con Narumi en la habitación 106.

**Nicolás Zepeda:**  Nos despertamos a las 7 de la mañana. Vi que empezó a prepararse para salir, pero el tiempo estaba malo y prefirió quedarse conmigo en la pieza. Tomamos desayuno y nos quedamos dormidos viendo una película. Entonces escuchamos que había gente tocando la puerta.

**Narrador:** 

Al otro lado estaba Arthur del Piccolo, preocupado por unos extraños mensajes que había recibido de su novia. Zepeda cuenta que Narumi le hizo un gesto para que se quedara callado. El reencuentro habría terminado algunas horas después, en la madrugada del 6 de diciembre, cuando Narumi supuestamente se arrepintió de su infidelidad y le pidió que se retirara.

Los testimonios de Zepeda fueron puestos a prueba constantemente por el juez, la fiscalía y los querellantes. ¿Por qué no habían hallado envases de condones en el basurero si hubo relaciones sexuales? ¿Por qué los vecinos de Narumi no escucharon ninguna actividad en su habitación el 5 de diciembre si estaban viendo películas? Zepeda optaba por respuestas ambiguas y, en algunos casos, por explicaciones difíciles de creer, como haberse llevado consigo los paquetes vacíos de condones por respeto a una inexistente costumbre japonesa.

¿La contradicción final de su versión de ese día? Después del emocionante reencuentro narrado por Zepeda, éste nunca volvió a contactar a Narumi Kurosaki.  
      
      `,
    },
    {
      audio: `https://interactivo.latercera.com/zepeda-assets/v2/CAP%206%20FX.mp3`,
      audiointro: `https://interactivo.latercera.com/zepeda-assets/INTRO%20CAP%206.mp3`,
      length: '5:41',
      prefix: `Episodio 6`,
      title: `Evaporada`,
      description: `Las autoridades de la universidad tardaron en reportar la desaparición de Narumi. Esta demora causó que la policía local empezara su investigación recién una semana después del encuentro con Zepeda. El chileno no fue su primer sospechoso.`,
      coordinates: {
        x: 2774,
        y: 1987,
      },
      script: `
**Narrador:** 

Arthur del Piccolo golpeó la puerta de la habitación 106 de la residencia Theodor Rousseau cerca de las 11 de la noche. Era 5 de diciembre, el día después del reencuentro entre Narumi y Zepeda.. Unos mensajes de su novia lo tenían desorientado. Ella parecía preocupada. Cuando estaba por pedirle al conserje que abriera la puerta recibió el siguiente correo de Narumi:

**Narumi Kurosaki:** Quería hablar contigo en persona, pero no puedo más. Me encontré con otro chico, pasé el día con él; no sé si me gusta. Por favor perdóname. Mañana te explicaré todo. Besos.

**Narrador:** 

Del Piccolo no insistió en entrar a la pieza y regresó confundido a su residencia, entre la tristeza y la rabia. Al día siguiente intentó hablar con ella, pero Narumi se negó. Le pedía “un espacio”. Entonces intentó conmoverla con un mensaje más extenso y romántico. El encabezado era “mi pequeña Narumi”…

**Narumi Kurosaki:** No soy tu pequeña Narumi.

**Narrador:** 

La situación le pareció cada vez más extraña. Entonces se enteró de que varios amigos de Narumi habían recibido mensajes suyos. En ellos les explicaba que había viajado a Lyon para renovar su visa. Era extraño, porque el consulado japonés estaba en Estrasburgo. Del Piccolo se acercó a Shintaro Obata, compatriota y amigo de Narumi, quien vivía en la habitación 107 de la residencia Rousseau. Obata estaba preocupado porque Narumi había faltado a clases y ya no contestaba los mensajes en japonés, solo en francés. Juntos comenzaron a juntar información y notificaron a la universidad. Empezaron a creer que su celular estaba intervenido. Finalmente, el 12 de diciembre, consiguieron entrar a la pieza de Narumi con la ayuda del encargado de la residencia.

**Arthur del Piccolo:**  Vi su abrigo y su bufanda. Era incomprensible que fuera a Lyon sin el abrigo.

**Narrador:** 

La policía solo entró a la habitación tres días después, el 15 de diciembre. Las autoridades de la residencia se habían demorado en alertar al departamento de relaciones internacionales y ellos, a su vez, habían tardado en hacer la denuncia. Parecía todo en orden: el refrigerador estaba lleno; su ropa, maquillaje y cuadernos estaban en su lugar; su computador estaba en un bolso; la billetera tenía 565 euros. Al repasar el inventario de sus posesiones, los investigadores se dieron cuenta de que solo faltaban una frazada y una maleta.

Al día siguiente, dudando que se tratara de una desaparición voluntaria, la Fiscalía local decidió abrir una investigación por posible rapto, secuestro y asesinato.

Las primeras miradas de la policía se dirigieron a Arthur Del Piccolo.

**David Borne:**  Su discurso parecía demasiado preparado. Era muy preciso al hablar, algo frío. No quisimos detenerlo de inmediato. Al final cambiamos de idea porque no había evidencia material. Revisamos su teléfono y su computador, pero no encontramos nada.

**Narrador:** 

La policía rastreó las actividades de Narumi usando las señales emitidas por la antena de su celular. Así llegaron a La Table de Gustave, en Ornans. Les mostraron fotos de ella y de Arthur del Piccolo.Los meseros reconocieron a la japonesa y la administración del restorán buscó en sus registros. Entonces se encontraron con que la comida había sido pagada con una tarjeta de Chile. La remota posibilidad de que el exnovio chileno estuviera involucrado era real.

La policía volvió a interrogar a varios amigos de Narumi para saber más de Nicolás Zepeda. Varios coincidieron en que se trataba de una relación que había terminado muy mal. Posteriormente identificaron el Renault Scenic que había registrado exceso de velocidad entre Ornans y Besanzón.

**David Borne**: Sabemos que hay una cámara en ese camino que es una trampa para los extranjeros. Revisamos el registro y tuvimos la suerte de encontrar este auto que pertenecía a una agencia. El contrato estaba firmado por un hombre llamado Nicolás Zepeda.

**Narrador:** 

Al tirar de esa hebra, las autoridades pudieron reconstruir el itinerario de Zepeda con relativa rapidez. Posteriormente, en las revisiones más exhaustivas de la habitación de Narumi, lo que se encontró fue tan llamativo como lo que faltaba. Aunque no había rastros de sangre, se descubrieron huellas dactilares en una taza, que más tarde coincidirían con las de Zepeda. A partir de esta muestra, se realizó un análisis genético que permitió identificar un ADN masculino que también estaba presente en una botella de agua, una polera, la muralla, el suelo del baño y el borde del lavamanos.

Cuando la policía hizo estos descubrimientos ya era muy tarde. Zepeda había dejado Besanzón, pasado por Barcelona y ya estaba de vuelta en Chile.
      
      `,
    },
    {
      audio: `https://interactivo.latercera.com/zepeda-assets/v2/CAP%207%20FX.mp3`,
      audiointro: `https://interactivo.latercera.com/zepeda-assets/INTRO%20CAP%207.mp3`,
      length: '4:16',
      prefix: `Episodio 7`,
      title: `En lo profundo del bosque`,
      description: `Apenas unas horas después de dejar la residencia de Narumi, el auto de Zepeda fue geolocalizado en un bosque cerca de Besanzón. Las autoridades especulan que los restos de la japonesa terminaron en ese lugar, pero hay un dilema: no tienen cómo probarlo.`,
      coordinates: {
        x: 2130,
        y: 590,
      },
      script: `
**David Borne:**  Hemos alcanzado la certeza de que Narumi está muerta. Desde el 4 de diciembre de 2016 no ha dado señales de vida ni ha sido vista en ninguna parte. La ausencia de transferencias bancarias, la presencia de efectivo en la billetera que dejó en su habitación, sus proyectos y la falta de señales de depresión también nos hacen descartar un suicidio.

**Narrador:** 

El investigador David Borne llegó tempranamente a la misma conclusión que muchos de los demás involucrados en el caso de Narumi Kurosaki. Era la explicación más probable en el gran contexto. ¿Por qué una joven aparentemente feliz querría escapar de todo de un momento a otro? El centro del misterio permanecía en las 30 horas que Nicolás Zepeda pasó en la pieza de su ex.

El chileno dice que salió de la residencia en la madrugada y que caminó sin rumbo por un período indeterminado mientras esperaba un mensaje de Narumi. Al entender que este no llegaría, se devolvió y recogió el auto a las 4.23 am. Habría manejado como por inercia hacia Dijon y sin querer se habría encontrado de pronto por los mismos caminos rurales que había recorrido el 1 de diciembre. Estuvo ahí casi dos horas.

**Nicolás Zepeda:**  Espero el mensaje de Narumi, no llega y decido empezar a manejar hasta donde llegue. Ya conozco la ruta, voy a una iglesia que ya había visitado y luego sigo a Dijon.

**Narrador:** 

Cuando en el juicio le hicieron ver 8que las iglesias estaban cerradas a esa hora, Zepeda apuntó a un error de traducción al francés: en realidad había dicho que durmió “cerca de la iglesia”.

**Nicolás Zepeda:** Estar cerca de las iglesias me hace sentir seguro. Es un hábito familiar.

**Narrador:** 

Más allá de estos detalles, toda la teoría de la parte acusadora apunta a que Zepeda actuó de forma premeditada; primero habría realizado un reconocimiento de esa zona buscando un lugar conveniente para deshacerse del cuerpo de su exnovia y luego habría llevado a cabo este plan tras salir de la residencia. El gran problema de esta tesis, y de toda la acusación contra el chileno, es la ausencia del cadáver de Narumi Kurosaki.

Durante el juicio, Borne mostró un mapa con el área de 50 kilómetros cuadrados donde se realizó la búsqueda de los posibles restos de la estudiante japonesa. La zona incluye un pedazo del bosque Chaux y el río Loue, uno de los principales afluentes del Doubs.

**David Borne:** Exploramos la ribera completa del Doubs con un sonar y chequeamos con buzos hasta una planta de tratamiento de aguas.

**Narrador:** 

David Borne agrega que complementaron la búsqueda con perros, pero que estos solo pueden rastrear material orgánico; no es suficiente, por ejemplo, que solamente queden los huesos. A la larga, todos estos esfuerzos no dieron resultado. Hasta hoy todavía no se encuentra ni un mínimo rastro biológico de Narumi en la zona. Aún existen diversas teorías respecto a cómo Zepeda se podría haber deshecho del cuerpo: lanzándolo al río, quemándolo con el combustible que compró en el supermercado o, en menor medida, enterrándolo. Ninguna ha podido ser corroborada.

La esperanza de los investigadores es que los restos aparezcan tarde o temprano, como ocurrió en otro caso de alto perfil ocurrido en la zona: la desaparición de la pequeña Maelys de Araujo, de ocho años. El descubrimiento del cuerpo, sin embargo, solo se hizo gracias a la confesión del asesino. En el caso Narumi, ya sea porque no sabe o porque no quiere, Zepeda está lejos de dar esa información.  
      
      `,
    },
    {
      audio:
        'https://interactivo.latercera.com/zepeda-assets/v2/CAP%208%20FX.mp3',
      audiointro: `https://interactivo.latercera.com/zepeda-assets/INTRO%20CAP%208.mp3`,
      length: '6:19',
      prefix: `Episodio 8`,
      title: `Palabras prestadas`,
      description: `La familia Kurosaki recibió mensajes extraños de Narumi en los días posteriores a su encuentro con Zepeda. Su manera de escribir no parecía natural. ¿Era realmente ella? Por esas mismas fechas, Zepeda también estaba preocupado de cómo expresarse en japonés.`,
      coordinates: {
        x: 1760,
        y: 3917,
      },
      script: `
**Narrador:** 

Al mismo tiempo que en Besanzón empezaban a dudar de quién estaba realmente detrás del celular de Narumi Kurosaki, su familia también comenzó a recibir mensajes que les parecían extraños. No era solo por el contenido, sino por giros idiomáticos inusuales para un hablante nativo de japonés. Su hermana Kurumi notó que confundía los signos silábicos de las jotas con las “i griegas”. Entre el 10 y 11 de diciembre, Narumi les contó a su madre y hermanas que se iba de paseo a Luxemburgo. Algunas de sus últimas frases en el chat familiar les sonaban curiosas.

**Narumi Kurosaki:**  Voy en el tren, no tengo Wifi.

**Narumi Kurosaki:**  Tengo un nuevo novio.

**Narumi Kurosaki:**  Me voy sola. 

**Narrador:**  

Recién en marzo de 2017, la fiscalía pudo afirmar su teoría acerca de Narumi y sus comunicaciones posteriores al encuentro con Zepeda. La policía japonesa interrogó a dos amigas que el chileno había dejado en ese país tras su estancia en la Universidad de Tsukuba: Rina Sakamaki y Megumi Sugihara. En los días siguientes a la desaparición de Narumi, ambas habían estado en contacto con él; Zepeda les había pedido ayuda para traducir al japonés coloquial frases como “voy en el tren, no tengo Wifi”, “tengo un nuevo novio” y “me voy sola”. En su primera declaración, que data de 2017, Sakamaki se mostró dolida.

**Rina Sakamaki:**  Yo me pregunté en ese momento por qué Nicolás deseaba aprender una frase cuyo estilo pertenece al lenguaje femenino (…) Al ver esta imagen me pregunté si mi traducción no habría sido utilizada por Nicolás para disimular la desaparición de Narumi. Eso me indignó.

**Narrador:** 

Las dos habían recibido también solicitudes de Zepeda para que borraran sus historiales de conversación en Facebook Messenger y Line. Sakamaki lo hizo de inmediato; Sugihara, en cambio, le preguntó por qué quería borrar sus conversaciones. Ambos tenían una relación más cercana. Ella, de hecho, había estudiado un año en Chile y vivido unos meses con una hermana de Zepeda. A pesar de esa cercanía, su amigo no le respondió.

Sugihara se enteró de la desaparición de Narumi 10 días después. Entonces volvió a conversar con Zepeda, quien parecía muy interesado en la cobertura mediática del caso en Japón. Según consta en el expediente, Sugihara volvió a preguntar por qué tenía que borrar el historial. La respuesta del chileno fue sorprendente.

**Nicolás Zepeda:**  No quiero ser el sospechoso de su desaparición por el solo hecho de que yo era su novio (…) No te preocupes, seguramente anda divirtiéndose con otros hombres.

**Narrador:** 

Arthur del Piccolo conversó con Taeko Akiyama y sus hijas sobre los extraños mensajes que todos habían recibido. Kurumi intuyó que Zepeda estaba involucrado. Entonces, su madre repasó varios de los correos electrónicos que había intercambiado con Narumi en los meses previos.

Durante el juicio, leyó algunos de ellos, reproduciendo las palabras de su hija.

**Taeko Akiyama:** 27 de septiembre…

**Narumi Kurosaki (citada por su madre):** Yo no sé si quiero a Nico, pero él me quiere mucho a mí. No sé cómo dejarlo. Siento piedad por él. Y cada vez que salgo con un hombre, me siento mal por Nico.

**Taeko Akiyama:** 28 de septiembre…

**Narumi Kurosaki (citada por su madre):** Parece que Nico está desesperado. Perdió la cabeza. Dijo que vendría a Francia y le dije que no.

**Taeko Akiyama:**  ¿Sabe dónde vives?

**Narumi Kurosaki (citada por su madre):**  Sí.

**Taeko Akiyama:**  De seguro irá. No te fíes, es Nico; no se sabe cómo puede actuar.

**Taeko Akiyama:** 10 de octubre

**Narumi Kurosaki (citada por su madre):** Nico se metió en mi Facebook para borrar contactos y leer los mensajes con mis amigos. También borró mis fotos con otros hombres.

**Narrador:** 

Las autoridades francesas también se convencieron de que Zepeda suplantó a Narumi usando su tarjeta de crédito para comprar un pasaje de tren entre Besanzón y Lyon el 6 de diciembre. Este hecho calzaba con la información del supuesto viaje que sus amigos habían recibido. Lo cierto es que Narumi nunca abordó el tren, según indica el registro de pasajeros. La dirección IP del celular ubicó al usuario que realizó la compra en el centro comercial Toison d’Or de Dijon, justamente donde la policía tenía geolocalizado a Zepeda en ese momento. Algo similar ocurrió con la compra de una VPN y con las conexiones de Narumi a redes sociales. Muchas de ellas coincidían con donde se encontrara Zepeda, en Barcelona o Santiago.

Al ser consultado, Zepeda contestó que compartía varias aplicaciones con Narumi y que al activarlas en su celular podía haber ubicado el celular de ella donde él estuviera.

Todas las comunicaciones de Narumi con familiares y amigos cesaron el 13 de diciembre; la última conexión a su cuenta de Google también fue el 13 de diciembre. Esta fecha coincide con el arribo de Nicolás Zepeda a Chile.
      
      `,
    },
    {
      audio:
        'https://interactivo.latercera.com/zepeda-assets/v2/CAP%209%20FX.mp3',
      audiointro: `https://interactivo.latercera.com/zepeda-assets/INTRO%20CAP%209.mp3`,
      length: '4:43',
      prefix: `Episodio 9`,
      title: `Visita familiar`,
      description: `Antes de regresar a Chile, Zepeda pasó cinco días en Barcelona junto a su primo Juan Felipe Ramírez. Al llegar, le pidió a su anfitrión que no comentara su visita con nadie. Ramírez intuyó que pasaba algo raro cuando le hicieron unas desconcertantes preguntas.`,
      coordinates: {
        x: 1133,
        y: 1626,
      },
      script: `
**Narrador:** 

La negativa de Juan Felipe Ramírez se informó en la primera jornada del juicio de Nicolás Zepeda, su primo hermano. Su nombre estaba en la lista de testigos porque su testimonio había sido relevante en los primeros meses de investigación. Quizás no quiso enfrentarse una vez más a su familia; ya había entregado una declaración problemática de lo ocurrido cuando alojó a Zepeda en Barcelona, entre el 8 y el 12 de diciembre de 2016, los días posteriores a la desaparición de Narumi.

Tras su periplo en Besanzón y alrededores, Zepeda devolvió el Renault Scenic en Dijon hacia el mediodía del 6 de diciembre. El vehículo había recorrido 776 kilómetros en ocho días de arriendo y lucía muy sucio, con tierra en el asiento del conductor y en la maleta. Esa misma tarde, compró un boleto de bus a Ginebra y otro de avión a Barcelona. Recién entonces le pidió a Ramírez, quien vivía con su esposa y estudiaba una especialidad médica, que lo alojara en su casa.

De acuerdo con la declaración que Ramírez le brindaría después a la justicia francesa en enero de 2017, Zepeda le explicó que había viajado a Europa para reemplazar a un profesor de su universidad en un congreso desarrollado en Ginebra. También le comentó que quería conocer alternativas para continuar sus estudios en la ciudad, pero que no quería que su papá se enterara. Entonces le pidió que no comentara su visita con nadie ni subiera nada a redes sociales. Los investigadores franceses vieron aquí un intento por ocultar su paso por Francia.
      
**Nicolás Zepeda:** No quería que mi padre se enterara que estaba en Europa buscando opciones universitarias. Yo quería contarle en persona. Si Juan Felipe le contaba a su hermana Catalina, ella se lo contaría a mis hermanas y se enteraría mi padre. Mi padre se habría enojado si se enteraba que estaba en Europa. En el pasado ya se había enojado cuando fui a Japón.

**Narrador:** 

Ramírez sabía del quiebre con Narumi, así que trató de preguntar con tacto; su primo contestó que no la veía desde septiembre y que estaba saliendo con una joven alemana.

Luego, Ramírez también recordó una conversación inquietante que ocurrió el sábado 10 de diciembre. En medio de la comida, Zepeda mostró una repentina curiosidad respecto a la muerte por asfixia. Este preguntó por qué muere una persona que se ha ahorcado, cuánto se demora en morir y cómo saber si la personas están vivas o muertas después de la horca. Los testimonios chocan nuevamente en este punto.

**Nicolás Zepeda:**  No recuerdo haber hecho esas preguntas.

**Narrador:** 

A su primo también le llamó la atención que hablara de su expolola indistintamente en presente y pasado. Recuerda haber oído, por ejemplo, que “a Narumi le GUSTABA mucho el mar”.

Ramírez se molestó al enterarse de la desaparición de la estudiante japonesa y de la mentira de su primo. Pronto entendió que estaba entre los sospechosos. Cuando habló de este tema con Zepeda, le dijo que la familia debía ayudarse en momentos complicados. La frase le sonó a amenaza. Entonces decidió declarar ante la Interpol catalana. Entre lo que reveló estaban las averiguaciones jurídicas que Zepeda y su familia habían realizado: no existía tratado de extradición entre Chile y Francia. Entre los documentos entregados por Ramírez a las autoridades estaba la siguiente conversación.

**Nicolás Zepeda:**  Tengo suficientes pruebas para defenderme. Tengo el apoyo de mi familia y estoy tranquilo.

**Juan Felipe Ramírez:**  Es horrible su desaparición.

**Nicolás Zepeda:**  Como de película.

**Juan Felipe Ramírez:**  Se contactó la familia de Narumi contigo?

**Nicolás Zepeda:**  No y yo tampoco lo hice.

Zepeda dejó Barcelona el 12 de diciembre y regresó a Ginebra para conectar con un vuelo a Madrid, desde donde finalmente viajó de vuelta a Santiago. Su reingreso al país está fechado el 13 de diciembre. Exactamente 10 días después, la justicia francesa emitió la orden de detención en su contra.
      `,
    },
    {
      audio:
        'https://interactivo.latercera.com/zepeda-assets/v3/CAP%201O%20OK.mp3',
      length: '5:18',
      prefix: `Episodio 10`,
      title: `La maquinaria judicial`,
      description: `Zepeda logró refugiarse en Chile antes de que la policía francesa exigiera su detención. Desde entonces, su vida quedó sujeta a un proceso internacional y en manos de juristas de alto perfil como Jacqueline Laffont, exdefensora de Nicolas Sarkozy.`,
      coordinates: {
        x: 757,
        y: 2908,
      },
      script: `
**Narrador:** Poco antes de Año Nuevo, el 29 de diciembre de 2016, Nicolás Zepeda presentó una carta con una declaración voluntaria a la PDI. Por casi cuatro años, esta fue su única versión de lo ocurrido. El documento le pesa a su defensa hasta hoy por la cantidad de inconsistencias que sus acusadores han encontrado respecto de sus versiones más recientes. En pocas palabras, se cree que su testimonio solamente se “acomodó” a las primeras informaciones que habían aparecido en los medios.

Mientras las propiedades de su familia en Santiago y La Serena comenzaban a ser rodeadas por periodistas japoneses, Zepeda contaba al menos con la tranquilidad de saber que no existía un tratado con Francia y que cualquier proceso de extradición sería lento. En febrero de 2017, confirmó que el panorama era auspicioso con la decisión de la Corte Suprema de rechazar la orden de arresto solicitada por la fiscal Ximena Chong en representación de Francia. Zepeda quedó solo con arraigo nacional por dos meses. El ministro Jorge Dahm juzgó que las pruebas presentadas eran insuficientes.

Francia volvió a la carga casi dos años y medio después, en abril de 2019. El fiscal general de Besanzón, Etienne Manteaux, viajó con una delegación hasta Santiago para interrogar a Zepeda, pero este guardó silencio en cada una de las 95 preguntas y se retiró desafiante a bordo de un Porsche. El persecutor francés intentó ver el lado positivo; la instancia sirvió para aunar esfuerzos con la Fiscalía de Chile para insistir por la extradición.

La solicitud oficial se presentó en octubre y las audiencias se celebraron en marzo de 2020. Zepeda tuvo a los conocidos penalistas Joanna Heskia y Pelayo Vial como representantes, pero esta vez la evidencia era más contundente. En abril, el máximo tribunal tomó su decisión.

**Jorge Dahm:**  Existen antecedentes con fundamento serio, cierto y grave, que permiten sostener que la última persona que estuvo con Narumi fue Nicolás. Después de ello ella desaparece. Mantuvieron una relación afectiva que terminó de mala manera tiempo antes, ruptura que no fue aceptada por Nicolás, dado los términos en que se refieren los múltiples mensajes que le dirigió.

**Narrador:** Con la extradición aprobada, la familia Zepeda se armó con una de las mejores abogadas de Francia para enfrentar el proceso criminal: Jacqueline Laffont. En su lista de clientes figuran personajes como el empresario Jean-Marie Messier, el exministro del Interior francés Charles Pasqua, el expresidente de Costa de Marfil Laurent Gbagbo y el expresidente de Francia Nicolas Sarkozy. La penalista había representado al exmandatario en un caso de corrupción y tráfico de influencias en el cual se le acusaba de ofrecerle un cargo en Monaco a un juez a cambio de información sobre otra de sus causas judiciales por financiamiento ilegal de campañas.

Laffont no pudo salvar a Sarkozy de una condena en ese caso y sus primeras diligencias en favor de Zepeda tampoco fueron exitosas. Su intento por impedir el juicio fue rechazado en abril de 2021, por lo que su equipo apostó todo al juicio, donde trataron de introducir dudas razonables a los integrantes de jurado en medio de la enorme evidencia indiciaria presentada por la Fiscalía. Con argumentos sencillos, intentó voltear las pruebas de la fiscalía para transmitir la idea de que Zepeda solo estuvo en el lugar y momento equivocado.

**Jacqueline Laffont:**  Mi representado reservó todo con su nombre verdadero e hizo sus compras con su tarjeta. Si te quisieras esconder, por estar planeando la muerte de una joven, ¿habrías dejado tus huellas en todos lados? La pregunta es un poco estúpida, lo siento.

**Narrador:** Laffont trató de anular las contradicciones testimoniales de su cliente apuntando a la solidez de sus gritos de inocencia. Incluso pareció rogarle al jurado por una oportunidad.

**Jacqueline Laffont:**  En sus respuestas que desconciertan, no hay defensa. Todo lo contrario. Nicolas Zepeda no sabe mentir. No engaña a nadie. Sólo hubo un momento de sinceridad: cuando gritó con lágrimas que él no la mató.

**Narrador:** 

A minutos del veredicto, otra vez entre lágrimas, Zepeda se paró delante de todos para reiterar lo que ha dicho desde hace seis años.

**Nicolás Zepeda:**  Yo no soy la persona que me gustaría ser. Yo no soy un asesino. No soy el asesino de Narumi.  
      
      `,
    },
    {
      prefix: `Epílogo`,
      audio:
        'https://interactivo.latercera.com/zepeda-assets/v2/EPILOGO%20CON%20MUSICA%20FINAL.mp3',
      length: '2:36',
      title: `Culpable.`,
      description: ``,
      coordinates: {
        x: 0,
        y: 0,
        zoom: 0.2,
      },
      script: `
**Narrador:**
Las miradas de tres países estaban sobre el pequeño tribunal de Bezansón el martes 12 de abril de 2022. Periodistas de Chile, Francia y Japón copaban el pasillo que daba ingreso a la sala donde podían permanecer poco más de 50 personas para escuchar el fallo del jurado de nueve personas, que sellaría el destino de Nicolás Zepeda.

En la solemnidad del momento, esa sala reunió a los protagonistas de una trama dolorosa. Zepeda y sus padres. La familia de Narumi. Los abogados. Todos pendientes de una resolución cuya lectura se anticipaba como breve, pero con consecuencias relevantes.

Momentos antes de las tres de la tarde en Francia, y de las nueve de la mañana en Chile, se conoció el veredicto. El jurado consideró como culpable a Nicolás Zepeda por el asesinato de Narumi Kurosaki. Además, estimó que, pese a que nunca se encontró el cuerpo de la joven estudiante japonesa, las pruebas eran suficientes para estimar que su expareja había actuado con premeditación.

El veredicto incluyó la pena de cárcel solicitada para Zepeda: 28 años. Una decisión que aún puede ser apelada a tribunales superiores en Francia, pero que se convierte en el primer fallo de este caso, que lleva más de cinco años de recovecos judiciales.

La resolución está lejos de ser el final del caso. Más allá de la condena, quedan muchas incógnitas aún pendientes por resolver. ¿Cómo ocurrió la muerte de Narumi Kurosaki? ¿Dónde fue dejado su cuerpo? ¿Y por qué no hubo testigos ni registros de lo que pasó en esas horas del 5 y 6 de diciembre de 2016?

Son preguntas que seguirán pendientes, en la mente de familias, abogados, jueces y periodistas, que continuarán intentando develar los detalles de lo que pasó realmente en la habitación 106.
      
      `,
    },
  ],
}

export default data
