import ActaAgate from './ActaAgate-Book.woff'
import ActaAgate2 from './ActaAgate-Book.woff2'

import ActaAgateB from './ActaAgate-Bold.woff'
import ActaAgateB2 from './ActaAgate-Bold.woff2'

import ActaBook from './Acta-Book.woff'
import ActaBook2 from './Acta-Book.woff2'

import ActaBold from './Acta-Bold.woff'
import ActaBold2 from './Acta-Bold.woff2'

import ActaDisplay from './ActaDisplay-ExtraBold.woff'
import ActaDisplay2 from './ActaDisplay-ExtraBold.woff2'

import ActaHeadlineBook from './ActaHeadline-Book.woff'
import ActaHeadlineBook2 from './ActaHeadline-Book.woff2'

import ActaHeadlineBookItalic from './ActaHeadline-BookItalic.woff'
import ActaHeadlineBookItalic2 from './ActaHeadline-BookItalic.woff2'

import AxiformaBold from './Axiforma-Bold.woff'
import AxiformaBold2 from './Axiforma-Bold.woff2'

import AxiformaBook from './Axiforma-Book.woff'
import AxiformaBook2 from './Axiforma-Book.woff2'

import InterRegular from './Inter-Regular.woff'
import InterRegular2 from './Inter-Regular.woff2'

import InterBold from './Inter-Bold.woff'
import InterBold2 from './Inter-Bold.woff2'

import InterExtraBold from './Inter-ExtraBold.woff'
import InterExtraBold2 from './Inter-ExtraBold.woff2'

export {
  ActaAgate,
  ActaAgate2,
  ActaAgateB,
  ActaAgateB2,
  ActaDisplay,
  ActaDisplay2,
  ActaBook,
  ActaBook2,
  ActaBold,
  ActaBold2,
  ActaHeadlineBook,
  ActaHeadlineBook2,
  ActaHeadlineBookItalic,
  ActaHeadlineBookItalic2,
  AxiformaBold,
  AxiformaBold2,
  AxiformaBook,
  AxiformaBook2,
  InterRegular,
  InterRegular2,
  InterBold,
  InterBold2,
  InterExtraBold,
  InterExtraBold2,
}
